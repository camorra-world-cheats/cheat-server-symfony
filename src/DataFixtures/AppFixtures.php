<?php

namespace App\DataFixtures;

use App\Entity\App;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $app = new App();
        $app->setUserIdentifier("user-identifier-fixture");
        $app->setFirebaseToken("firebase-token-fixture");

        $manager->persist($app);

        $manager->flush();
    }
}
